const int relayPin = 2;
const int buttPin  = 4;

int rele_state   = 0;
int butt_state   = 0;
String ser_input = "a";
String dev_name  = "ArduSwitcher";

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial.setTimeout(200);
  
  pinMode(relayPin, OUTPUT);
  pinMode(buttPin, INPUT);  

  Serial.print("Relay PIN : ");
  Serial.println(relayPin);

  Serial.print("Button PIN: ");
  Serial.println(buttPin);
}

void loop() {
  butt_state = digitalRead(buttPin);
  rele_state = digitalRead(relayPin);
  
  if (Serial.available() > 0) {
    ser_input = Serial.readString();
    ser_input.trim();
    
    Serial.print("Got input: ");
    Serial.println(ser_input);
    
    if (ser_input == "on") {
      turnon();
    }

    if (ser_input == "off") {
      turnoff();
    }

    if (ser_input == "dev_name") {
      Serial.println(dev_name);
    }

    if (ser_input == "ping") {
      Serial.write("pong");
    }
  }
  
  if(butt_state == HIGH) {
    if (rele_state == LOW) {
      turnon();
      delay(1000);
    } else {
      turnoff();
      delay(1000);
    }
  }
}

void turnon() {
  digitalWrite(relayPin, HIGH);
}

void turnoff() {
  digitalWrite(relayPin, LOW);
}
